// User Collection

db.users.insertMany([
	{
		"firstName" : "Diane",
		"lastName" : "Murphy",
		"email" : "dmuprhy@mail.com",
		"isAdmin" : false,
		"isActive" : true
	}, 
	{
		"firstName" : "Mary",
		"lastName" : "Patterson",
		"email" : "mpatterson@mail.com",
		"isAdmin" : false,
		"isActive" : true
	}, 
	{
		"firstName" : "Jeff",
		"lastName" : "Firrelli",
		"email" : "jfirrelli@mail.com",
		"isAdmin" : false,
		"isActive" : true
	}, 
	{
		"firstName" : "Gerard",
		"lastName" : "Bondur",
		"email" : "gbondur@mail.com",
		"isAdmin" : false,
		"isActive" : true
	}, 
	{
		"firstName" : "Pamela",
		"lastName" : "Castillo",
		"email" : "pcastillo@mail.com",
		"isAdmin" : true,
		"isActive" : false
	}, 
	{
		"firstName" : "George",
		"lastName" : "Vanauf",
		"email" : "gvanauf@mail.com",
		"isAdmin" : true,
		"isActive" : true
	}
])

// Course Collection
db.courses.insertMany([
	{
		"name" : "Professional Development",
		"price" : 10000.00
	},
	{
		"name" : "Business Processing",
		"price" : 13000.00
	}

])

// Enrollees for the Courses
db.courses.updateOne(
	{
       "_id" : ObjectId("6125b524cc69e0ec7332800b")
	},
	{
		$set: { "enrollees": "Murphy, Firelli"}
	}
	
)

db.courses.updateOne(
	{
       "_id" : ObjectId("6125b524cc69e0ec7332800c")
	},
	{
		$set: { "enrollees": "Bondur, Patterson"}
	}
	
)

// Not Administrators
db.users.find(
	{
	"isAdmin" : false
	}
)

// Checking 
db.users.find()
db.courses.find()